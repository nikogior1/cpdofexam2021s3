package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testFunctMax() throws Exception {
        int k= new MinMax().funct(1,2);
	    assertEquals("TestFunct",2,k);
    }
    @Test
    public void testFunctMin() throws Exception {
        int k= new MinMax().funct(2,1);
	    assertEquals("TestFunct2",2,k);
    }
    @Test
    public void testBarNull() throws Exception {
        String k= new MinMax().bar(null);
	    assertEquals("testBarNull", null, k);
    }
    @Test
    public void testBarEmpty() throws Exception {
        String k= new MinMax().bar("");
	    assertEquals("testBarNull", "", k);
    }
    @Test
    public void testBarFull() throws Exception {
        String k= new MinMax().bar("lol");
	    assertEquals("testBarNull", "lol", k);
    }
}

